<?php

namespace Neon\Slideshow\Services;

use Neon\Slideshow\Models\Slideshow;
use Illuminate\Database\Eloquent\Collection;

class SlideshowService
{
  /**
   * @param string $id UUID of the given slideshow.
   * @return Slideshow
   */
  public static function find(string $id): Slideshow
  {
    return Slideshow::where('id', '=', $id)->with('items')->first();
  }
}
