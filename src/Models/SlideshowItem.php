<?php

namespace Neon\Slideshow\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Neon\Attributable\Models\Traits\Attributable;
use Neon\Models\Basic as BasicModel;
use Neon\Models\Statuses\BasicStatus;
use Neon\Models\Traits\Publishable;
use Neon\Models\Traits\Statusable;
use Neon\Models\Traits\Uuid;
use Neon\Site\Models\Traits\SiteDependencies;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;


class SlideshowItem extends BasicModel implements HasMedia, Sortable
{
	use Attributable;
	use InteractsWithMedia;
	use LogsActivity;
	use SoftDeletes;
	use SortableTrait;
	use Statusable;
	use Uuid;

	// N30N UUID to forget auto increment stuff.

	const MEDIA_SLIDE = 'slideshow_item';
	const MEDIA_SLIDE_MOBILE = 'slideshow_item_mobile';
	/** Set up sorting.
	 *
	 * @var array
	 */
	public $sortable = [
		'order_column_name'  => 'order',
		'sort_when_creating' => true,
		'sort_on_has_many'   => true,
	];
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'title',
		'lead',
		'cta_text',
		'cta_link',
		'status',
		'order',
		'published_at',
		'expired_at',
	];
	protected $casts = [
		'title'      => 'string',
		'lead'       => 'string',
		'cta_text'   => 'string',
		'cta_link'   => 'string',
		'status'     => BasicStatus::class,
		'order'      => 'integer',
		'created_at' => 'datetime',
		'updated_at' => 'datetime',
		'deleted_at' => 'datetime',
	];

	public function getActivitylogOptions(): LogOptions
	{
		return LogOptions::defaults();
	}

	public function registerMediaCollections(): void
	{
		$this->addMediaCollection(self::MEDIA_SLIDE)->singleFile();
		$this->addMediaCollection(self::MEDIA_SLIDE_MOBILE)->singleFile();
	}

	public function registerMediaConversions(Media $media = null): void
	{
		$this->addMediaConversion('thumb')
			->height(100)
			->fit(\Spatie\Image\Manipulations::FIT_MAX, 100, 100)
			->optimize()
			->performOnCollections(self::MEDIA_SLIDE);

		$this->addMediaConversion('medium')
			->height(600)
			->fit(\Spatie\Image\Manipulations::FIT_MAX, 600, 600)
			->optimize()
			->performOnCollections(self::MEDIA_SLIDE);

		$this->addMediaConversion('thumb')
			->height(100)
			->fit(\Spatie\Image\Manipulations::FIT_MAX, 100, 100)
			->optimize()
			->performOnCollections(self::MEDIA_SLIDE_MOBILE);

		$this->addMediaConversion('medium')
			->height(600)
			->fit(\Spatie\Image\Manipulations::FIT_MAX, 600, 600)
			->optimize()
			->performOnCollections(self::MEDIA_SLIDE_MOBILE);
  }

	public function buildSortQuery()
	{
		return static::query()
			->where('slideshow_id', $this->slideshow_id);
	}

}
