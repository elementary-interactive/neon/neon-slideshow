<?php

namespace Neon\Slideshow\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Neon\Attributable\Models\Traits\Attributable;
use Neon\Models\Traits\Uuid;
use Neon\Models\Traits\Publishable;
use Neon\Models\Traits\Statusable;
use Neon\Models\Basic as BasicModel;
use Neon\Site\Models\Traits\SiteDependencies;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;


class Slideshow extends BasicModel
{
  use Attributable;
  use LogsActivity;
  use Publishable;
  use SiteDependencies;
  use SoftDeletes;
  use Statusable;
  use Uuid; // N30N UUID to forget auto increment stuff.

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'title',
    'status',
    'published_at',
    'expired_at'
  ];

  protected $casts = [
    'title'         => 'string',
    'created_at'    => 'datetime',
    'updated_at'    => 'datetime',
    'deleted_at'    => 'datetime',
    'published_at'  => 'datetime',
    'expired_at'    => 'datetime'
  ];

  public function getActivitylogOptions(): LogOptions
  {
    return LogOptions::defaults();
  }

  public function items(): HasMany
  {
    return $this->hasMany(SlideshowItem::class);
  }
}
