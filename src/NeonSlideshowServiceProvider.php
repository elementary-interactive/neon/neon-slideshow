<?php

namespace Neon\Slideshow;

use Illuminate\Foundation\Console\AboutCommand;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;

class NeonSlideshowServiceProvider extends PackageServiceProvider
{
	const VERSION = '1.0.4';

	public function configurePackage(Package $package): void
	{
		AboutCommand::add('N30N', 'Slideshow', self::VERSION);

		$package
			->name('neon-slideshow')
			->hasConfigFile()
			->hasMigration('create_slideshows_table')
			->hasMigration('create_slideshow_items_table');
	}
}
