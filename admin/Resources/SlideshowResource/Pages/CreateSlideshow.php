<?php

namespace Neon\Admin\Resources\SlideshowResource\Pages;

use Neon\Admin\Resources\SlideshowResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateSlideshow extends CreateRecord
{
    protected static string $resource = SlideshowResource::class;
}
