<?php

namespace Neon\Admin\Resources\SlideshowResource\RelationManagers;

use Filament\Forms;
use Filament\Forms\Components\SpatieMediaLibraryFileUpload;
use Filament\Forms\Form;
use Filament\Resources\RelationManagers\RelationManager;
use Filament\Tables;
use Filament\Tables\Columns\SpatieMediaLibraryImageColumn;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;
use Neon\Models\Statuses\BasicStatus;
use Neon\Slideshow\Models\SlideshowItem;

/**
 * Class SlideshowItemsRelationManager
 * @package Neon\Admin\Resources\SlideshowResource\RelationManagers
 */
class SlideshowItemsRelationManager extends RelationManager
{
	/**
	 * @var string
	 */
	protected static string $relationship = 'items';

	/**
	 * @var string|null
	 */
	protected static ?string $recordTitleAttribute = 'title';

	/**
	 * @param Form $form
	 * @return Form
	 */
	public function form(Form $form): Form
	{
		return $form
			->schema(
				[
					Forms\Components\TextInput::make('title')
						->label(__('neon-admin::admin.resources.slideshow_items.form.fields.title.label'))
						->helperText(__('neon-admin::admin.resources.slideshow_items.form.fields.title.help'))
						->columnSpanFull(),
					SpatieMediaLibraryFileUpload::make('slideshow-item')
						->label(__('neon-admin::admin.resources.slideshow_items.form.fields.media.label'))
						->collection(SlideshowItem::MEDIA_SLIDE)
						->responsiveImages(),
					SpatieMediaLibraryFileUpload::make('slideshow-item-mobile')
						->label(__('neon-admin::admin.resources.slideshow_items.form.fields.media.label'))
						->collection(SlideshowItem::MEDIA_SLIDE_MOBILE)
						->responsiveImages(),
					Forms\Components\Textarea::make('lead')
						->label(__('neon-admin::admin.resources.slideshow_items.form.fields.lead.label'))
						->rows(4)
						->columnSpanFull(),
					Forms\Components\TextInput::make('cta_text')
						->label(__('neon-admin::admin.resources.slideshow_items.form.fields.cta_text.label'))
						->helperText(__('neon-admin::admin.resources.slideshow_items.form.fields.cta_text.help')),
					Forms\Components\TextInput::make('cta_link')
						->label(__('neon-admin::admin.resources.slideshow_items.form.fields.cta_link.label'))
						->helperText(__('neon-admin::admin.resources.slideshow_items.form.fields.cta_link.help'))
						->activeUrl(),
					Forms\Components\Select::make('status')
						->label(__('neon-admin::admin.resources.slideshow_items.form.fields.status.label'))
						->required()
						->native(false)
						->default(BasicStatus::default())
						->options(BasicStatus::class),
				]
			);
	}

	/**
	 * @param Table $table
	 * @return Table
	 */
	public function table(Table $table): Table
	{
		return $table
			->columns([
						  SpatieMediaLibraryImageColumn::make('slideshow-item')
							  ->label(__('neon-admin::admin.resources.slideshow_items.form.fields.media.label'))
							  ->collection(SlideshowItem::MEDIA_SLIDE),
						  Tables\Columns\TextColumn::make('title')
							  ->label(__('neon-admin::admin.resources.slideshow_items.form.fields.lead.label'))
							  ->description(fn(SlideshowItem $record): string => "{$record->lead}"),
						  Tables\Columns\TextColumn::make('cta_text')
							  ->label(__('neon-admin::admin.resources.slideshow_items.form.fields.cta_text.label')),
						  Tables\Columns\TextColumn::make('cta_link')
							  ->label(__('neon-admin::admin.resources.slideshow_items.form.fields.cta_link.label')),
						  Tables\Columns\IconColumn::make('status')
							  ->label(__('neon-admin::admin.resources.slideshow.form.fields.status.label'))
							  ->icon(fn(BasicStatus $state): string => match ($state) {
								  BasicStatus::New      => 'heroicon-o-sparkles',
								  BasicStatus::Active   => 'heroicon-o-check-circle',
								  BasicStatus::Inactive => 'heroicon-o-x-circle',
								  BasicStatus::Draft    => 'heroicon-o-x-circle',
							  })
							  ->color(fn(BasicStatus $state): string => match ($state) {
								  BasicStatus::New      => 'gray',
								  BasicStatus::Active   => 'success',
								  BasicStatus::Inactive => 'danger',
								  BasicStatus::Draft    => 'danger',
							  })
							  ->searchable()
							  ->sortable(),
						  Tables\Columns\TextColumn::make('published_at'),
						  Tables\Columns\TextColumn::make('expired_at'),
					  ])
			->reorderable('order')
			->defaultSort('order')
			->filters([
						  Tables\Filters\TrashedFilter::make(),
						  // ...
					  ])
			->headerActions([
								Tables\Actions\CreateAction::make(),
							])
			->actions([
						  Tables\Actions\EditAction::make(),
						  Tables\Actions\DeleteAction::make(),
						  Tables\Actions\ForceDeleteAction::make(),
						  Tables\Actions\RestoreAction::make(),
					  ])
			->groupedBulkActions([
									 Tables\Actions\DeleteBulkAction::make(),
								 ])
			->modifyQueryUsing(fn(Builder $query) => $query->withoutGlobalScopes());
	}
}
