<?php

use Neon\Models\Statuses\BasicStatus;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
  /**
   * Run the migrations.
   */
  public function up()
  {
    Schema::create('slideshows', function (Blueprint $table) {
      $table->uuid('id'); // We are using UUID as primary key.

      $table->string('title');

      $table->char('status', 1)
        ->default(BasicStatus::default()->value);

      $table->timestamp('published_at')
          ->nullable()
          ->default(null);
      $table->timestamp('expired_at')
          ->nullable()
          ->default(null);

      $table->timestamps();
      $table->softDeletes();

      $table->primary('id');
    });
  }

  /**
   * Reverse the migrations.
   */
  public function down()
  {
      Schema::dropIfExists('slideshows');
  }

};