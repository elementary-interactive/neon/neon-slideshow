<?php

use Neon\Models\Statuses\BasicStatus;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
  /**
   * Run the migrations.
   */
  public function up()
  {
    Schema::create('slideshow_items', function (Blueprint $table) {
      $table->uuid('id'); // We are using UUID as primary key.

      $table->uuid('slideshow_id');

      $table->string('title')
        ->nullable()
        ->default(null);
      $table->text('lead')
        ->nullable()
        ->default(null);

      $table->string('cta_text')
        ->nullable()
        ->default(null);
      $table->string('cta_link')
        ->nullable()
        ->default(null);

      $table->char('status', 1)
        ->default(BasicStatus::default()->value);

      $table->tinyInteger('order', false, true);

      $table->timestamps();
      $table->softDeletes();

      $table->primary('id');
      $table->foreign('slideshow_id')->references('id')->on('slideshows');
      $table->index('status');
      $table->index('deleted_at');
    });
  }

  /**
   * Reverse the migrations.
   */
  public function down()
  {
      Schema::dropIfExists('slideshow_items');
  }

};